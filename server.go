package main

import (
	"bufio"
	"log"
	"net"
	"os"
	"strings"
)

// defaults
// TODO:Config file
const defaultContentDir string = "/var/gopher/"
const defaultDomain string = "localhost:7070"
const defaultConfigFileLocation string = "/etc/wafflehouse"

type config struct {
	ContentDir string
	Domain     string
}

func fileToConfig(s string) *config {
	cfg := &config{}
	if strings.TrimSpace(s) == "" {
		return nil
	}
	f, err := os.Open(s)

	if err != nil {
		return nil
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		text := scanner.Text()
		// allow for comments
		if text[0] == '#' {
			continue
		}

		if strings.HasPrefix(text, "Domain:") {
			_, d, _ := strings.Cut(text, "Domain:")
			(*cfg).Domain = strings.TrimSpace(d)
		} else if strings.HasPrefix(text, "ContentDir:") {
			_, c, _ := strings.Cut(text, "ContentDir:")
			(*cfg).ContentDir = strings.TrimSpace(c)
		} else {
			log.Println("Config: Unknown setting:", text)
		}
	}

	if *cfg == (config{}) {
		return nil
	}
	return cfg

}

// args could be made local and passed here?
// probably would be renamed to "handle gopher connection" or smething
func handleConnection(conn net.Conn, cfg *config) {
	isGopherMap := false
	defer conn.Close()

	// we only need to read 1 string here so this is fine
	path, _ := bufio.NewReader(conn).ReadString('\n')

	path = strings.TrimSpace(path)             // remove newline
	path = strings.ReplaceAll(path, "../", "") // prevent basic path traversal exploit

	log.Println("User is going to", path)

	file := cfg.ContentDir + path

	ft, err := os.Stat(file)
	if err != nil {
		conn.Write([]byte("3Cannot access content\tfake\tnull.host\t1\r\n.\r\n"))
		conn.Close()
		return // fix error below
	}

	// this gives a nil pointer error, causing a panic
	if ft.Mode().IsDir() {
		file = file + "/gophermap"
		isGopherMap = true
	}

	data, err := os.ReadFile(file)

	if err != nil {
		conn.Write([]byte("3Cannot access content\tfake\tnull.host\t1\r\n.\r\n"))
		conn.Close()
		return
	}

	conn.Write(data)
	if isGopherMap {
		conn.Write([]byte("\r\n.\r\n"))
	}

	log.Println("Closing connection")
	return
}

func serveAndHandle(cfg *config) error {
	ln, err := net.Listen("tcp", cfg.Domain)

	if err != nil {
		log.Fatalln(err)
	}

	// main logic
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
		} else {
			go handleConnection(conn, cfg)
		}
	}
}

func main() {
	file := defaultConfigFileLocation
	if len(os.Args) > 1 {
		file = os.Args[1]
	}

	cfg := fileToConfig(file)

	if cfg == nil {
		cfg = &config{
			ContentDir: defaultContentDir,
			Domain:     defaultDomain,
		}
	}
	log.Println("Config generated from file:", file)
	log.Println("Serving with following config:", *cfg)
	serveAndHandle(cfg)
}
