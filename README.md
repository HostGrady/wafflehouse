# Wafflehouse

Wafflehouse is a gopher server, I designed it as a personal project to help me work on another personal project
I decided to release it to the public just because why not.

## Limitations

This is a very simplistic gopher server which means more advanced features such as full text search are not implemented.
It cannot do cgi either, it is just intended to serve files as the gopher protocol was intended for.

## Why the name wafflehouse?

Gaufre is how you say waffle in French, I found it funny. Sadly gaufre was taken by another client.

My logic is that the users are getting gopher (gaufre) content from the server, hence wafflehouse.

## Licensing

Unlicense, all code contributions must be under unlicense or they will be ignored.
